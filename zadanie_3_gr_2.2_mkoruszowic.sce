/*
Matlab applications in physics
Author: Michał Koruszowic
Technical Physics
*/

//Please make sure that script is in the same location as folder "data" (which contains all data).

list_of_files =listfiles("data");  //making list of file names, "data" is name of file folder
imported_files = list(); //creating a list that will contain read files

// variable that will be used to find max PM10 concentration
max_pm10 = 0;
index_pm10 = 0;

//loop going through each file
for i=1:size(list_of_files,1) 
    imported_files(i)=csvRead('data\'+list_of_files(i),";", [], "string", [",", "."]); //adding imported files to list
    [max_temp,index_temp]=max(strtod(imported_files(i)(2:25,6))); // searching for the max PM10 concentration in particular file
    if max_temp > max_pm10 //checking if max from given data is bigger than max from previous data
        max_pm10 = max_temp;
        index_pm10 = [i, index_temp + 1] ; //remembering index of max
    end
    
    //finding index of specific time
    index_1 = find(imported_files(i)(:,1)=="1:00");
    index_7 = find(imported_files(i)(:,1)=="7:00");
    index_18= find(imported_files(i)(:,1)=="18:00");
    index_24= find(imported_files(i)(:,1)=="24:00");
    
    //time range was taken from https://meteogram.pl/slonce/polska/gliwice/
    //dawn: 8:00 - 17:00
    //dusk: 1:00 - 7:00, 18:00 - 24:00
    
    //calculating mean vaule for dusk and dawn in each file
    dusk = mean(strtod(imported_files(i)([index_1:index_7,index_18:index_24],6)));
    dawn = mean(strtod(imported_files(i)([index_7+1:index_18-1],6)));
    
    
    diff_matrix(i) =  dawn - dusk; //diffrence between dusk and dawn
end

//saving time and data of the maximum concetration
time_pm10 = imported_files(index_pm10(1))(index_pm10(2),1);
date_pm10 = part(list_of_files(index_pm10(1)), [16:25]);

mean_diff=mean(diff_matrix); // calculating mean of differences

student_fisher = 1.090; // Student-Fischer coefficient for 7 measurements was taken from http://if.pk.edu.pl/tabele/wSF.htm
diff_stdev = stdev(diff_matrix) / sqrt(size(list_of_files,1)) * student_fisher; //calculating standard deviation of the mean difference

test = abs(mean_diff) / diff_stdev ; // conducting Welch's test

//checking the date of the first and last file (analyzed period)
first_date = part(list_of_files(7), [16:25]); 
last_date =part(list_of_files(1), [16:25]);

fd = mopen('PMresults.dat', 'w'); //creating a text file named "PMresults.dat" and opening it

//displaying maximum concentration of PM10 in the command window and writing it to the file
mfprintf(fd,"The maximum concentration of PM10 was on %s at %s and amounted to %d [µg/m3]. \n",date_pm10, time_pm10, max_pm10);
printf("The maximum concentration of PM10 was on %s at %s and amounted to %d [µg/m3]. \n",date_pm10, time_pm10, max_pm10);


//checking the result of the test, displaying comment in the command window and writing it to the file
if test >= 3
    
    mfprintf(fd,"There is a statistical difference in concentration of PM10 for dawn and dusk, the test was %.2f \n", test)
    printf("There is a statistical difference in concentration of PM10 for dawn and dusk, the test was %.2f \n", test);

elseif  test >= 2 & test <= 3
    
    mfprintf(fd,"Unable to determine if there is a significant statistical difference in concentration of PM10 for dusk and dawn, the test was %.2f \n", test)
    printf("Unable to determine if there is a significant statistical difference in concentration of PM10 for dusk and dawn, the test was %.2f \n", test);

else
    
    mfprintf(fd,"There is no statistical difference in concentration of PM10 for dawn and dusk, the test was %.2f \n", test)
    printf("There is no statistical difference in concentration of PM10 for dawn and dusk, the test was %.2f \n", test);

end

//displaying period of analyzed data in the command window and writing it to the file
mfprintf(fd,"Analyzed period: %s to %s.", first_date, last_date);
printf("Analyzed period: %s to %s.", first_date, last_date);

mclose(fd); //closing opened file


